# pinniped-arm

build image for arm64 for this project:
https://github.com/vmware-tanzu/pinniped.git

# GO options

Ref: https://github.com/mlnoga/nightlight/issues/4

The RasPis have an ARM CPU running in 32 bit mode under Linux. Pi1 is ARM6, Pi2 and 3 are ARM7, Pi4 is ARM8. However, commonly distributed OSs like Raspbian are still ARM7 in 32 bit mode. While ARM supports both little and big endian operations, the RasPis run in little endian mode like x86 CPUs.

Hence: GOOS=linux GOARCH=arm GOARM=7 go build

# manuel build

COMPOSE_DOCKER_CLI_BUILD=1 DOCKER_BUILDKIT=1 docker build -t registry.gitlab.com/etangs/pinniped-arm/pinniped-server:v0.12.0 .


# auth to k8s via gitlab oidc

https://pinniped.dev/docs/howto/install-supervisor/
https://pinniped.dev/docs/howto/configure-supervisor-with-gitlab/
https://pinniped.dev/docs/howto/configure-concierge-supervisor-jwt/
https://pinniped.dev/docs/howto/login/
